package com.codegym.cms.service;

import com.codegym.cms.model.Cart;


public interface CartService {
    Iterable<Cart> findAll();

    Cart findById(Long id);

    void save(Cart cart);

    void remove(Long id);
    Iterable<Cart> findAllByIsDeletedEquals(int isDeleted);
}
