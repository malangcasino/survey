package com.codegym.cms.service;

import com.codegym.cms.model.Category;
import com.codegym.cms.model.Customer;
import com.codegym.cms.model.Product;
import com.codegym.cms.model.Province;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    Iterable<Product> findAll();

    Customer findById(Long id);

    void save(Product product);

    void remove(Long id);

    Iterable<Product> findAllByProvince(Category category);

    Iterable<Product> findAllByIsDeletedEquals(int isDeleted);
    Page<Product> findAll(Pageable pageable);




}
