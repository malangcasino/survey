package com.codegym.cms.repository;

import com.codegym.cms.model.Category;

public interface CategoryRepository {
    Iterable<Category> findAllByIsDeletedEquals(int isDeleted);
}
