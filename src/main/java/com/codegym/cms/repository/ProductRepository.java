package com.codegym.cms.repository;

import com.codegym.cms.model.Category;
import com.codegym.cms.model.Product;

import com.codegym.cms.model.Province;
import org.springframework.data.repository.PagingAndSortingRepository;



public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Iterable<Product> findAllByIsDeletedEquals(int isDeleted);
    Iterable<Category> findAllByProvince(Category category);

}
